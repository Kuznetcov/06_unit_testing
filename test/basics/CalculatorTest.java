package basics;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

@RunWith(Parameterized.class)
public class CalculatorTest {

  @Parameters
  public static Collection<String> data() {
    return Arrays.asList("5+5", "5/0", "5+2.5", "A + B", "5 **5");
  }
  private String source;

  public CalculatorTest(String iteration) {
    source = iteration;
  }

  @Test
  public void calculateParameter() {
    @SuppressWarnings("unused")
    Calculator cal = new Calculator();
    System.out.println("Тестируем: "+source);
    Calculator.calculate(source);
  }


/* 
  //Те же самые тесты. Нарушаем принцип DRY, но зато названия тестов более очевидны
  @Test
  public void calculateTrueEval() {
    @SuppressWarnings("unused")
    Calculator cal = new Calculator();
    String source = "5+5";
    Calculator.calculate(source);
  }

  @Test
  public void calculateDivisionZero() {

    String source = "5/0";
    Calculator.calculate(source);
  }

  @Test
  public void calculateWrongEval() {

    String source = "5+2.5";
    Calculator.calculate(source);
  }

  @Test
  public void calculateChars() {

    String source = "A + B";
    Calculator.calculate(source);
  }

  @Test
  public void calculateDoubleOp() {

    String source = "5 **5";
    Calculator.calculate(source);
  }
*/

}
