package basics;

import org.junit.Test;
import static org.junit.Assert.*;

public class PolskTest {

  @Test
  public void multiplyOp (){
    
    @SuppressWarnings("unused")
    Polsk polsk = new Polsk();
    
    int result = Polsk.eval("(5 + 5 * -5 / 5 - 10)");
    assertEquals(-10,result);
  }
}
