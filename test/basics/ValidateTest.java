package basics;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;

public class ValidateTest {

  @Test
  public void objectCreation() {
    Validate valid = new Validate("проверка");
    String info = valid.info;
    assertNotSame("",info);
  }

  @Test
  public void validateNotInteger() {
    Validate valid = new Validate("2.5 + 2.5");
    String info = valid.info;
    assertThat(info, containsString("Error"));
  }

  @Test
  public void validateAllRight() {
    Validate valid = new Validate("5 + 5");
    String info = valid.info;
    boolean flag = valid.correct;
    assertTrue(info.equals("") && flag == true);
  }
}
