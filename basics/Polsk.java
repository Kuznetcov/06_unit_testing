package basics;

import java.util.LinkedList;

public class Polsk {

  private static boolean isDelim(char c) {
    return (c == ' ' || c == '(' || c == ')');
  }

  private static boolean isOperator(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/';
  }

  private static int priority(char op) {
    switch (op) {
      case '+':
      case '-':
        return 1;
      case '*':
      case '/':
        return 2;
      default:
        return -1;
    }
  }

  private static void processOperator(LinkedList<Integer> st, char op) {

    int r = st.removeLast();
    int l = st.removeLast();
    switch (op) {

      case '+':
        st.add(l + r);
        break;
      case '-':
        st.add(l - r);
        break;
      case '*':
        st.add(l * r);
        break;
      case '/':
        st.add(l / r);
        break;
      default:
        return;
    }
  }

  public static int eval(String s) throws ArithmeticException {
    LinkedList<Integer> st = new LinkedList<Integer>();
    LinkedList<Character> op = new LinkedList<Character>();
    boolean flag = true;
    String operand = "";
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (isDelim(c)) {
        continue;
      }

      if (isOperator(c)) {

        if (flag && c == '-') {
          operand = "-";
        } else {

          while (!op.isEmpty() && priority(op.getLast()) >= priority(c)) {
            processOperator(st, op.removeLast());
          }
          op.add(c);
          flag = true;
        }
      } else {

        while (i < s.length() && Character.isDigit(s.charAt(i))) {
          operand += s.charAt(i++);
          
        }
        --i;
        st.add(Integer.parseInt(operand));
        operand = "";
        flag = false;
      }
    }

    while (!op.isEmpty()) {
      processOperator(st, op.removeLast());
    }
    return st.get(0);
  }
}
