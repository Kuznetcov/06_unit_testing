package basics;

import java.util.NoSuchElementException;

public class Calculator {

  public static void calculate(String args) {
    Validate result = new Validate(args);
    if (result.correct == false) {
      System.out.print(result.info);
    } else {

      try {
        Parser2.parse(args);
        int out = Parser2.dump.get(args);
        System.out.println("Введенное выражение: " + args);
        System.out.println(Parser2.dump);
        System.out.println("Результат: " + out);
      } catch (ArithmeticException x) {
        System.out.println("Обнаружена ошибка: деление на ноль.");
      } catch (NoSuchElementException x){
        System.out.println("Обнаружена ошибка: двойной операнд");
      }
    }
  }
}

